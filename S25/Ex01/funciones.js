

addEventListener('load', inicializarEventos, false);

function inicializarEventos(){
    
    for(var f=1; f<=12; f++){
        var ob=document.getElementById('enlace'+f);
        ob.addEventListener('click', presionEnlace, false);
    }
}

function presionEnlace(e){
    
    e.preventDefault();
    var url=e.target.getAttribute('href');
    cargarHoroscopo(url);
    
}


var conexion;

function cargarHoroscopo(url){
    conexion= new XMLHttpRequest();
    conexion.onreadystatechange=procesarEventos;
    conexion.open("GET", url, true);
    conexion.send();
}


function procesarEventos(){
    var detalles = document.getElementById("detalles");
    if(conexion.readyState === 4){
        detalles.innerHTML = conexion.responseText;
    }
    else{
        detalles.innerHTML = 'Cargando...';
    }
}