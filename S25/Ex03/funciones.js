

addEventListener('load', inicializarEventos, false);

function inicializarEventos(){
    
    var ob=document.getElementById('boton');
    ob.addEventListener('click',presionarBoton,false);
}

function presionarBoton(e){
    
    var ob1=document.getElementById('voto');
    var ob2=document.getElementById('nombre');
    cargarVoto(ob1.value,ob2.value);

}


var conexion;

function cargarVoto(voto,nombre){
    conexion= new XMLHttpRequest();
    conexion.onreadystatechange=procesarEventos;
    conexion.open('GET', 'pag.php?&votos='+voto+'&nombre='+nombre, true);
    conexion.send();
}


function procesarEventos(){
    var resultados = document.getElementById("resultados");
    if(conexion.readyState === 4){
        resultados.innerHTML = conexion.responseText;
    }
    else{
        resultados.innerHTML = 'Cargando...';
    }
}