<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PeliculasController extends Controller
{
    public function login(){
        return view('home');
    }
    
    public function logout(){
        return "Log out usuario!";
    }
    
    public function catalog(){
        return view('catalog.index');
    }
    
    
    public function show($id){
        return view('catalog.show', ['id' => $id]);
    }
    
    public function create($id){
        return view('catalog.create', ['id' => $id]);
    }
    
    public function edit($id){
        return view('catalog.edit', ['id' => $id]);
    }
}
 