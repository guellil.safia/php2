<?php

class mostrar{
    
    private $db;
    
    private $codigo;
    private $descripcion;
    private $precio;
    private $codigorubro;
    
    public function __construct() {
        $this->db= Conectar::conexion();
        $this->codigo=array();
        $this->descripcion=array();
        $this->precio=array();
        $this->codigorubro=array();
    }
    
    function getCodigo() {
        $consulta = $this->db->query("select codigo from articulos");
        while($filas=$consulta->fetch_assoc()){
            $this->codigo[]=$filas;
        }
        return $this->codigo;
    }

    function getDescripcion() {
        $consulta = $this->db->query("select descripcion from articulos");
        while($filas=$consulta->fetch_assoc()){
            $this->descripcion[]=$filas;
        }
        return $this->descripcion;
    }

    function getPrecio() {
        $consulta = $this->db->query("select precio from articulos");
        while($filas=$consulta->fetch_assoc()){
            $this->precio[]=$filas;
        }
        return $this->precio;
    }

    function getCodigorubro() {
        $consulta = $this->db->query("select codigorubro from articulos");
        while($filas=$consulta->fetch_assoc()){
            $this->codigorubro[]=$filas;
        }
        return $this->codigorubro;
    }

}


class insertar{
    
    public $ok;
    private $db;
    public function verificar(){
        
        $this->db= Conectar::conexion();
        $consulta= $this->db->query("select * from rubros where descripcion='$_REQUEST[descripcion]'");
        
        
        if($consulta->fetch_array()) 
        { 
            $this->ok=true;
            
        }else{
            $this->db->query("insert into rubros(descripcion) values ('$_REQUEST[descripcion]')");
            $this->ok=false;
        }
        
        return $this->ok;
    } 
}


?>
