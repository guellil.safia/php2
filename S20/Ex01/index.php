<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
         class Empleado
         {
             //Atributos
             private $nombre;
             private $sueldo;
             
             public function __construct($nom, $suel) {
                 $this->nombre=$nom;
                 $this->sueldo=$suel;
             }
             
             public function imprimir(){
                 
                 if(($this->sueldo)>3000){
                     
                     echo 'Debes pagar impuestos';
                 }else{
                     echo 'No debes pagar impuestos';
                 }
                     
             }
         }
         
         $empleado1= new Empleado("José", 31);
         $empleado1->imprimir();
        ?>
    </body>
</html>
