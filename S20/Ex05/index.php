<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
         class Tabla
         {
            //Atributos
             private $tab=array();
             private $filas;
             private $columnas;
             private $colorFondo;
             private $ColorFuente;
             
             public function __construct($fi, $co) {
                $this->filas=$fi;
                $this->columnas=$co;
             }
            public function cargar($fil, $col, $val){
                $this->tab[$fil][$col]=$val;
            } 
             public function imprimir(){
                 
                echo '<table border="1">';
                
                 for($i=1; $i<=$this->filas; $i++){
                     echo '<tr>';
                     for($j=1; $j<=$this->columnas; $j++ ){
                         
                         $this->mostrar($i,$j);
                         
                     }
                     echo '</tr>';
                 }
                 
                echo '</table>';
                     
             }
             
             public function mostrar($fi, $col){
                 echo '<td>'.$this->tab[$fi][$col].'</td>';
             }
         }
         
         $tabla1= new Tabla(2,2);
         $tabla1->cargar(1,1, 2);
         $tabla1->cargar(1,2, 3);
         $tabla1->cargar(2,1, 4);
         $tabla1->cargar(2,2, 5);
         $tabla1->imprimir();
         
         
         
        ?>
    </body>
</html>
