<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
         class Menu
         {
             //Atributos
             private $orietacion;
             private $opciones;
             private $enlaces=array();
             private $titulo= array();
             
             public function __construct($orien, $op){
                 $this->orientacion=$orien;
                 $this->opciones=$op;
             }
             
             public function cargarOpcion($en, $tit) {
                 $this->enlaces[]=$en;
                 $this->titulo[]=$tit;
             }
             
             public function imprimir(){
                 
                 for($i=0; $i< $this->opciones; $i++ ){
                     echo '<a href ="'.$this->enlaces[$i].'"> '.$this->titulo[$i].' </a>';
                     if($this->orientacion=="vertical"){
                         echo '<br>';
                     }
                 }
                     
             }
         }
         
         $menu1 = new Menu("vertical",3);
         $menu1->cargarOpcion("google.es","Google");
         $menu1->cargarOpcion("gmail.es","Gmail");
         $menu1->cargarOpcion("spain.es","Spain");
         $menu1->imprimir();
        ?>
        
    </body>
</html>
