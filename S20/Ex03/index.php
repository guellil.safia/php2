<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
         class CabeceraPagina
         {
             //Atributos
             private $titulo;
             private $ubicacion;
             private $colorFuente;
             private $colorFondo;
             
             public function __construct($tit, $ubi='center', $cFuente='#D5361A', $cFondo='#EDE9B4') {
                 $this->titulo=$tit;
                 $this->ubicacion=$ubi;
                 $this->colorFuente=$cFuente;
                 $this->colorFondo=$cFondo;
             }
             
             public function imprimir(){
                 
                 echo '<div style = " background-color:'.$this->colorFondo.'"><h1 style="text-align:'.$this->ubicacion.'; color:'.$this->colorFuente.' " >'.$this->titulo.' </h1></div>';
                     
             }
         }
         
         $cabecera1 = new CabeceraPagina('Ejemplo 01 predeterminado');
         $cabecera1->imprimir();
         
         $cabecera2 = new CabeceraPagina('Ejemplo 02', 'right','#AA7A04' ,'#90E0D5');
         $cabecera2->imprimir();
         
         $cabecera3 = new CabeceraPagina('Ejemplo 03', 'left', '#AA7A04');
         $cabecera3->imprimir();
        ?>
    </body>
</html>
