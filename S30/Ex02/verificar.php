<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class verificar extends Controller
{
    public function mostrar(){
 
        return view('tabla', ['pais' => '0']);
        
    }
    
    
    
    public function guardar(Request $request){
        
        $pais = $request['pais'];
        
        
        
        switch ($pais){
            
            case 0:
                $arg= ['Argel', 'Kouba'];
                $region= $arg;
                $pais = 'Argelia';
                break;
            
            case 1:
                $esp=array('Madrid', 'Tarragona');
                $region=$esp;
                $pais = 'España';
                break;
            
            case 2:
                $jap=array('Tokio', 'Kyoto');
                $region= $jap;
                $pais = 'Japón';
                break;
            
        }
        $datos = [
          'pais' => $pais,
           'ciudad' =>$region
        ];
        return view('tabla')->with($datos);
        
    }
}
