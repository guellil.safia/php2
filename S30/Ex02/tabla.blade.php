<!DOCTYPE html>

<html>
    <head>
        <title>Paises</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    </head>
    <body>
       
        <div class="container" style="margin-top: 50px;">
            <div class="row">
                <div class="col">
                    
                    <form method="POST">
                        <h2>Selecciona el país:</h2><br>
                        <select name="pais" class="form-control">
                            <option value="0">Argelia</option>
                            <option value="1">España</option>
                            <option value="2">Japón</option>
                        </select>
                        <br>
                        <button type="submit" class="btn btn-primary">Eviar</button>
                        
                    </form><br>
                    <h4>Selecciona qué ciudad de {{$pais}}: </h4>
                    
                    
                    <select multiple  name="ciudad" class="form-control">
                        
                        @foreach ($ciudad as $reg)
                            <option>{{$reg}}</option>
                        @endforeach
                        
                    </select>
                </div>
            </div>
        </div>
            
        
        
        
    </body>
</html>
