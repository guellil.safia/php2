<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class verificar extends Controller
{
    public function mostrar(){
 
        return view('tabla', ['tiempo' => null]);
        
    }
    
    
    
    public function calcular(Request $request){
        
        $altura = $request['altura'];
        $radio = $request['radio'];
        $caudal = $request['caudal'];
        
        $volumen= $altura*$radio*$radio*pi();
        $tiempo = $volumen/$caudal;
        
        
        return view('tabla',['tiempo' => $tiempo]);
        
    }
}
