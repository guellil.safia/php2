<!DOCTYPE html>

<html>
    <head>
        <title>Paises</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    </head>
    <body>
       
        <div class="container" style="margin-top: 50px;">
            <div class="row">
                <div class="col">
                    
                    <form method="POST">
                        <h4>Introduce la altura del cilindro</h4>
                        <input type="number" name="altura"><br><br>
                          <h4>Introduce el diametro del cilindro</h4>
                        <input type="number" name="diametro"><br>  
                        
                        <br>
                        <button type="submit" class="btn btn-primary">Calcular</button>
                        
                    </form><br>
                    <h4>El volume del cilidro es: </h4>
                    
                    <h2 style="text-align: center;">{{$volumen}} metros</h2>
                       
                    
                    
                </div>
            </div>
        </div>
            
        
        
        
    </body>
</html>
