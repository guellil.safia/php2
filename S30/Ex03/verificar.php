<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class verificar extends Controller
{
    public function mostrar(){
 
        return view('tabla', ['volumen' => '0']);
        
    }
    
    
    
    public function calcular(Request $request){
        
        $altura = $request['altura'];
        $diametro = $request['diametro'];
        
        $radio = $diametro/2;
        $volumen= $radio*$radio*pi()*$altura;
        
        return view('tabla',['volumen' => $volumen]);
        
    }
}
