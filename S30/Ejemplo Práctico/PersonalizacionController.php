<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;



class PersonalizacionController extends Controller
{
    public function personalizar(){
        
     $tamanoFuente = cookie()->forever('fuente', '30pt');
     return view('personalizacion.formulario', ['fuente' => $tamanoFuente]);
        
    }
    
    public function guardarPersonalizacion(Request $request){
        
        $this->validate($request, ['fuente' => 'required']);
        
        return redirect('/personalizacion')
        ->withCookie(cookie('fuente', $request->input('fuente'), 60*24*365));
        
    }
    
}
 