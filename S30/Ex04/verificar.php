<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class verificar extends Controller
{
    public function mostrar(){
 
        return view('tabla', ['promedio' => null]);
        
    }
    
    
    
    public function calcular(Request $request){
        
        $prod1 = $request['tienda1'];
        $prod2 = $request['tienda2'];
        $prod3 = $request['tienda3'];
        
        $promedio = ($prod1+$prod2+$prod3)/3;
        
        
        return view('tabla',['promedio' => $promedio]);
        
    }
}
