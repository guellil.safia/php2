<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
  <!-- Brand -->
  <a class="navbar-brand" href="#">Logo</a>

  <!-- Links -->
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="{{action('PeliculasController@incluir')}}">Añadir Registro</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{action('PeliculasController@listar')}}">Listar</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Borrar</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Buscar</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Modificar</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Borrar Todo</a>
    </li>
    
  </ul>
</nav
