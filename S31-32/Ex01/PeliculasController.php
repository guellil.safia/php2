<?php

namespace App\Http\Controllers;
use App\Pelicula;
use Illuminate\Http\Request;

class PeliculasController extends Controller
{
    
    public function mostrar(){
        
        $pelicula= Pelicula::all();
        return $pelicula ;
    }
    
    public function listar(){
        
       $pelicula = $this->mostrar();
        
        return view('listar', ['peliculas' => $pelicula]);
        
    }
    
    public function vista(){
       
        return view('pelicula');
        
    }
    
    public function incluir(){
        
        $insertar = new Pelicula;
        dd($insertar);
        $insertar->codigo = 6;
        $insertar->nombre = 'Aladdin';
        $insertar->save();
        
        $pelicula= $this->mostrar();
        return view('incluir', ['peliculas' => $pelicula]);
    }
    
}
