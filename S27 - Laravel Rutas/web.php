<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//EJERCICIO 01

Route::get('/paises', function () {
     echo "Listado de paises!";
});
Route::post('/paises', function () {
     echo "Listado de paises!";
});
Route::delete('/paises', function () {
     echo "Has borrado la ruta país!";
});



Route::get('/paises/{pais}', function ($pais) {
     echo "El país seleccionado por GET es : $pais";
});
Route::post('/paises/{pais}', function ($pais) {
     echo "El país seleccionado por POST es : $pais";
});
Route::put('/paises/{pais}', function ($pais) {
     echo "El país seleccionado por PUT es : $pais";
});


Route::get('/paises/{pais}/departamentos', function ($pais) {
     echo "El país seleccionado por GET es : $pais dentro de departamentos";
});

Route::post('/paises/{pais}/departamentos', function ($pais) {
     echo "El país seleccionado por GET es : $pais dentro de departamentos";
});



Route::get('/paises/{pais}/departamentos/{departamento}', function ($pais, $departamento) {
     echo "El país seleccionado por GET es : $pais dentro de departamento: $departamento";
});

Route::put('/paises/{pais}/departamentos/{departamento}', function ($pais, $departamento) {
     echo "El país seleccionado por PUT es : $pais dentro de departamento: $departamento";
});

Route::delete('/paises/{pais}/departamentos/{departamento}', function ($pais, $departamento) {
     echo "El país seleccionado por GET es : $pais dentro de departamento: $departamento";
});

//EJERCICIO 02


Route::get('/colaboradores/{nombre}', function ($nombre) {
     echo "Nombre del colaborador por GET: $nombre";
});
Route::post('/colaboradores/{nombre}', function ($nombre) {
     return  "Nombre del colaborador por POST: $nombre";
});


Route::get('/tienda/productos/{num}', function ($num) {
     return "El numero del producto por GET: $num";
})->where(['id' => '[0]']);

