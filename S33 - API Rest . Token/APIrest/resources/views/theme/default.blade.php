<!DOCTYPE html>
<html lang="en">
    @include('theme.head')
<body>
   <div id="wrapper">
       <!-- Navigation -->
       <nav class="navbar navbar-default navbar-static-top" role="navigation" 
            style="margin-bottom: 0">
           @include('theme.header')
           @include('theme.sidebar')
       </nav>
       <div id="page-wrapper">
           @yield('content')
      </div>
       <!-- /#page-wrapper -->
   </div>
   
</body>
</html>
