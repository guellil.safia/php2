<?php

namespace App\Http\Middleware;

use Closure;

class LunesMiddleware
{
    
    public function handle($request, Closure $next)
    {
        if (date("w") == '1'){
            echo 'es lunes!';
        }else{
            echo 'no es lunes!';
        }
        
        return $next($request);
    }
}
