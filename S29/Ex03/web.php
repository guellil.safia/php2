<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function() {
    return view('welcome');
});



Route::group( ['middleware' => 'lunes'], function(){

   
    Route::get('/test/lunes',function(){
        return ' Prueba get';
        });

    Route::post('/test/lunes',function(){
        return ' Prueba post';
        });

    Route::put('/test/lunes',function(){
        return ' Prueba put';
        });

    Route::delete('/test/lunes',function(){
        return ' Prueba delete';
        });
    
    
});

