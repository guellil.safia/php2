create database empleadosDB;
use empleadosDB;


create table empleados(
	dni int ,
	nombre nvarchar(100),
	apellidos varchar(225),
	departamento int,
	primary key (dni),
	foreign key (departamento)
	references departamentos(codigo)
	on delete set null
	on update cascade

) engine=innodb;

create table departamentos(
	codigo int,
	nombre varchar(100),
	presupuesto int,
	primary key(codigo)
	
)engine = innodb;