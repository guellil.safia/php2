create database almacenesDB;
use almacenesDB;


create table almacenes(
	codigo int ,
	lugar nvarchar(100),
	capacidad int,
	primary key (codigo)
	

) engine=innodb;

create table cajas(
	numReferencia char(5),
	contenido varchar(100),
	valor int,
	almacen int,
	primary key(numReferencia),
	foreign key (almacen)
	references almacenes(codigo)
	on delete set null
	on update cascade
	
)engine = innodb;