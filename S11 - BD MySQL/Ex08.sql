create database piezasyProveedoresDB;
use piezasyProveedoresDB;


create table suministra(
	codigoPieza int,
	Idproveedores char(4),
	precio int,
	

	foreign key (codigoPieza)
	references piezas(codigo)
	on delete set null
	on update cascade,

	foreign key (Idproveedores)
	references proveedores(id)
	on delete set null
	on update cascade
	

) engine=innodb;

create table piezas(
	codigo int,
	nombre varchar(100) ,
	
	primary key(codigo)
	
	
)engine = innodb;

create table proveedores(
	id char(4),
	nombre varchar(100),
	primary key(id)
		
)engine = innodb;