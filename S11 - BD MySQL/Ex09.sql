create database CientificosDB;
use CientificosDB;


create table asignado_a(
	cientifico varchar(8),
	proyecto char(4),


	foreign key (cientifico)
	references cientificos(DNI)
	on delete set null
	on update cascade,

	foreign key (proyecto)
	references proyectos(id)
	on delete set null
	on update cascade
	

) engine=innodb;


create table cientificos(
	DNI varchar(8),
	nombreApels varchar(255) ,
	primary key(DNI)
)engine = innodb;

create table proyectos(
	id char(4),
	nombre varchar(255),
	horas int,
	primary key(id)
		
)engine = innodb;