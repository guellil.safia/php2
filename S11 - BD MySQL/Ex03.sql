create database tienda_informatica;
use tienda_informatica;


create table fabricantes(
	codigo int ,
	nombre nvarchar(100),
	primary key (codigo)

) engine=innodb;

create table articulos(
	codigo int,
	nombre varchar(100),
	precio int,
	fabricante int,
	primary key(codigo),
	foreign key (fabricante)
	references fabricantes(codigo)
	on delete set null
	on update cascade
)engine = innodb;