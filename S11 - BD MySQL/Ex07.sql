create database DirectoresDB;
use DirectoresDB;


create table directores(
	DNI varchar(8),
	nomApels varchar(255),
	DNIjefe varchar(8),
	despacho int,
	primary key (DNI),
	foreign key (DNIjefe)
	references directores(DNI)
	on delete set null
	on update cascade,
	foreign key (despacho)
	references despachos(numero)
	on delete set null
	on update cascade
	

) engine=innodb;

create table despachos(
	numero int,
	capacidad int,
	
	primary key(numero)
	
	
)engine = innodb;