create database peliculasySalasDB;
use peliculasySalasDB;


create table salas(
	codigo int ,
	nombre nvarchar(100),
	pelicula int,
	primary key (codigo),
	foreign key (pelicula)
	references peliculas(codigo)
	on delete set null
	on update cascade
	

) engine=innodb;

create table peliculas(
	codigo int,
	nombre varchar(100),
	calificacionEdad int,
	primary key(codigo)
	
	
)engine = innodb;