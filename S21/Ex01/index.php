<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
    
        <?php
            
            class operacion{
                protected $valor1;
                protected $valor2;
                protected $resultado;
                
                public function carga1($v1) {
                    $this->valor1=$v1;                    
                }
                
                public function carga2($v2) {
                    $this->valor2=$v2;
                }
                
                public function mostrarResultado(){
                    echo $this->resultado . '<br>';
                }        
            }
            
            class Suma extends operacion{
                
                public function operar(){
                    $this->resultado = $this->valor1 + $this->valor2;
                }
            }
            
            class Resta extends operacion{
                
                public function operar(){
                    $this->resultado = $this->valor1 - $this->valor2;
                }
            }
            
            $suma = new Suma();
            $suma->carga1(10);
            $suma->carga2(2);
            $suma->operar();
            $suma->mostrarResultado();
            
            $resta = new Resta();
            $resta->carga1(10);
            $resta->carga2(2);
            $resta->operar();
            $resta->mostrarResultado();
            
            
            
        ?>
    </body>
</html>
