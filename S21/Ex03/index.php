<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
    
        <?php
            
            class persona{
                protected $nombre;
                protected $edad;
                
                
                public function cargarDatos($nom, $ed) {
                    $this->nombre=$nom;
                    $this->edad=$ed;
                }
                
                
                
                public function mostrarResultados(){
                    echo 'La persona llamada '.$this->nombre . ' tiene '.$this->edad. 'edad <br>';
                }        
            }
            
            class Empleado extends persona{
                private $sueldo;
                
                public function cargaSueldo($su){
                    $this->sueldo=$su;
                }
                
                public function mostrarSueldo(){
                    echo 'El sueldo de '.$this->nombre.' es de '.$this->sueldo.' €<br>';
                }
            }
            
            
            
            $individuo= new persona();
            $individuo->cargarDatos('Safia', 20);
            $individuo->mostrarResultados();
            
            $trabajador= new Empleado();
            $trabajador->cargarDatos('José', 25);
            $trabajador->mostrarResultados();
            $trabajador->cargaSueldo(2156);
            $trabajador->mostrarSueldo();
            
            
        ?>
    </body>
</html>
