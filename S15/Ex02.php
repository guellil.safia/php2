<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 2</title>
    </head>
    <body>
        <?php
         $num = 0;
          echo 'BUCLE FOR';
         for($i=0;$i<11;$i++){
             $num=2*$i;
             echo "<br>$num";
         }
         echo '<br><br>BUCLE WHILE ';
         $num = 0;
         $i=0;
         while($i<11){
             $num=2*$i;
             echo "<br>$num";
             $i++;
         }
         
         echo '<br><br>BUCLE DO WHILE ';
         $num = 0;
         $i=0;
         do{
             $num=2*$i;
             echo "<br>$num";
             $i++;
         }
         while($i<11);
             
        ?>
    </body>
</html>

