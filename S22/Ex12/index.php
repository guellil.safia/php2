<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        abstract class  Trabajador{
            
            protected $nombre;
            protected $sueldo;
            
            public function __construct($nom) {
                $this->nombre=$nom;
            }
           
            
            abstract function calcularSueldo();
             
            public function mostrarResultados(){
                echo 'La persona llamada '.$this->nombre.' gana '.
                $this->sueldo.'€ <br>';
            }
            
            
        }
        
        class Empleado extends Trabajador{
            
            private $horasTrabajadas;
            private $valorHora;
            
            public function __construct($nom,$thoras, $vhoras) {
                parent::__construct($nom);
                $this->horasTrabajadas=$thoras;
                $this->valorHora=$vhoras;
            } 
                    
           public function calcularSueldo() {
               $this->sueldo= $this->horasTrabajadas*$this->valorHora;
           }
                   
        }
        
        class Gerente extends Trabajador{
            private $horasTrabajadas;
            private $valorHora;
            
            public function __construct($nom,$thoras, $vhoras) {
                parent::__construct($nom);
                $this->horasTrabajadas=$thoras;
                $this->valorHora=$vhoras;

           }
            public function calcularSueldo() {
                $this->sueldo= $this->horasTrabajadas*$this->valorHora;
                $this->sueldo = $this->sueldo+$this->sueldo*0.1;
           }
        
        }
        
        $emp= new Empleado('Juan', 48,3.5);
        $emp->calcularSueldo();
        $emp->mostrarResultados();
        
        $ger = new Gerente('Safia',48,3.5);
        $ger->calcularSueldo();
        $ger->mostrarResultados();
        
        
        ?>
    </body>
</html>
