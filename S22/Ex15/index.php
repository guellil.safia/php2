<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        
         class  Personaa{
            
            protected $nombre;
            protected $edad;
            
            public  function cargar($nom,$ed){
                $this->nombre=$nom;
                $this->edad=$ed;
            }
            
            public function mostrarNombre(){
                echo 'La persona se llama '.$this->nombre.'<br>';
            }
            
            public function mostrarEdad(){
                echo 'La persona tiene '.$this->edad.' años. <br>';
            }
        }
        
        $person = new Personaa();
        $person->cargar('Julio', 20);
        $person->mostrarEdad();
        $person->mostrarNombre();
        
        echo '<br>';
        
        $persona2=$person;
        $persona2->cargar('Julio', 30);
        $person->mostrarEdad();
        
        echo '<br>';
        
        $persona3= clone ($person);
        $persona3->cargar('Safia', 23);
        $person->mostrarNombre();
        $persona3->mostrarNombre();
        
        
        ?>
    </body>
</html>
