<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        abstract class  Persona{
            
            protected $nombre;
            protected $edad;
            
            public function __construct($nom,$ed){
                $this->nombre=$nom;
                $this->edad=$ed;
            }
            
            public function mostrarResultados(){
                echo 'La persona llamada '.$this->nombre.' tiene '.
                $this->edad.' años. <br>';
            }
            
            
        }
        
        class Empleado extends Persona{
            
            private $sueldo;
            
            public function __construct($nom,$ed, $suel) {
                
                Persona:: __construct($nom, $ed);
                $this->sueldo=$suel;
                
            }
            
            
            public function mostrarR(){
                Persona:: mostrarResultados();
                echo 'Gana '.$this->sueldo.' €';
            }
        }
        
        $pers = new Persona('Juan', 20);
        $pers->mostrarResultados();
        $emp= new Empleado('Juan', 25, 2541 );
        $emp->mostrarR();
        
        ?>
    </body>
</html>
