<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        abstract class Operacion{
            
            protected $valor1;
            protected $valor2;
            protected $resultado;


            public function carga1($v1){
                $this->valor1=$v1;
            }
            
            public function carga2($v2){
                $this->valor2=$v2;
            }
            
            public final function mostrarResultados(){
                echo $this->resultado.'<br>';
            }
            
            abstract function operar();
        
            
        }
        
        final class Sumar extends Operacion{
            
            public function __construct($v1, $v2) {
                
                Operacion:: carga1($v1);
                Operacion:: carga2($v2);
                
            }
            
            public function operar(){
                
                $this->resultado= $this->valor1+$this->valor2;   
            }  
        }
        

        
        $suma = new Sumar(5, 8);
        $suma->operar();
        $suma->mostrarResultados();
        
       
        
        
        ?>
    </body>
</html>
