<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        
         class  Persona{
            
            private $nombre;
            private $edad;
            
            public  function cargar($nom,$ed){
                $this->nombre=$nom;
                $this->edad=$ed;
            }
            
            public function mostrarResultados(){
                echo 'La persona se llama '.$this->nombre.'<br>';
                echo 'La persona tiene '.$this->edad.' años. <br>';
            }
            
            public function __clone(){
                $this->edad=$this->edad+1;
            }
        }
        
        $persona = new Persona();
        $persona->cargar('Julio', 20);
        $persona->mostrarResultados();
        
        
       
        
        
        
        echo '<br>';
        
        $persona3= clone($persona);
        $persona3->mostrarResultados();
       
        
        
        ?>
    </body>
</html>
