<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        class Operacion{
            
            protected $valor1;
            protected $valor2;
            protected $resultado;
            public function cargarValores($v1,$v2){
                $this->valor1=$v1;
                $this->valor2=$v2;
            }
            
            
        }
        
        class Suma extends Operacion{
            private $titulo;
            public function __construct($v1,$v2, $tit) {
                Operacion:: cargarValores($v1,$v2);
                $this->titulo=$tit;
            }
            public function operar(){
                
                echo $this->titulo;
                echo $this->valor1.'+'.$this->valor2.' es '.
                        $this->resultado=$this->valor1+$this->valor2;
                
            }
        }
        
        $suma = new Suma(5, 3, 'La suma de dos valores es: <br>');
        $suma->operar();
        
        ?>
    </body>
</html>
