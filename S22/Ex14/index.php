<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        
         class  Persona{
            
            protected $nombre;
            protected $edad;
            
            public  function __construct($nom,$ed){
                $this->nombre=$nom;
                $this->edad=$ed;
            }
            
            public final function mostrarResultados(){
                echo 'La persona llamada '.$this->nombre.' tiene '.
                $this->edad.' años. <br>';
            }
            
            
        }
        
        final class Empleado extends Persona{
            
            private $sueldo;
            
            public function __construct($nom,$ed, $suel) {
                
                Persona:: __construct($nom, $ed);
                $this->sueldo=$suel;
                
            }
            
            
            public function mostrarR(){
                Persona:: mostrarResultados();
                echo 'Gana '.$this->sueldo.' €';
            }
        }
        
        $perso = new Persona('Juan',28);
        
        $perso->mostrarResultados();
        
        $emp = new Empleado('Maria',25,3265);
        $emp->mostrarR();
        
        
        ?>
    </body>
</html>
